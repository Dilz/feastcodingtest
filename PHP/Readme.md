# Feast Coding Test - PHP #
For this part of the test you will need to build a 'path counting virtual matrix'.

## Instructions ##
Write a class that prints how many different paths can be taken to go from the top left to the bottom right in an X x Y virtual grid. You can only move right or down.

## Limitations ##
This should be done without using external libraries or frameworks.
