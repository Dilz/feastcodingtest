# Feast Coding Test - JavaScript #
For this part of the test you will need to build a character countdown.

## Instructions ##
You need to create a textarea with a live counter of a limit of 140 which indicates how many characters the user has remaining, this will also need to include 
a way to notify the user when the limit is reached. Please use Twitter as a reference.
You should then create a nice and clean user interface using the Twitter Bootstrap CSS Framework to display your character countdown.

## Limitations ##
- You can use any JaveScript framework you want (ideally jQuery). 
- The use of libraries and plugins will not be accepted.
