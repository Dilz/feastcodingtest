# Feast Coding Test #

This test is composed of two parts; PHP and JavaScript

## Instructions ##
Follow the instructions provided in each section. This is your chance to show your full range of abilities and skills, write the best code you can and show us the best practices you know.

## Timeframe ##
You have 24 hours to complete both sections.

## Git ##
You can demonstrate your git knowledge by sending back the code as a pull request on our bitbucket repository. * This is not compulsory
